import json
import falcon

from sqlalchemy.orm import sessionmaker

from .models import Group, UserAccount

class ResourceBase(object):
    def __init__(self, engine):
        self._db_engine = engine

    def error(errMsg):
        return { "error": errMsg }

class GroupsResource(ResourceBase):
    def on_get(self, req, res):
        session = sessionmaker(bind = self._db_engine)()
        res.status = falcon.HTTP_200
        res.body = json.dumps([ i.toDict() for i in session.query(Group).all() ])

    def on_post(self, req, res):
        if not req.content_length:
            return

        session = sessionmaker(bind = self._db_engine)()
        newGroup = Group(**req.media)
        session.add(newGroup)
        session.commit()

        res.status = falcon.HTTP_200
        res.body = json.dumps(newGroup.toDict())

class UsersResource(ResourceBase):
    def on_get(self, req, res):
        session = sessionmaker(bind = self._db_engine)()
        res.status = falcon.HTTP_200
        res.body = json.dumps([ i.toDict() for i in session.query(UserAccount).all() ])

    def on_post(self, req, res):
        if not req.content_length:
            return

        session = sessionmaker(bind = self._db_engine)()
        newGroup = UserAccount(**req.media)
        session.add(newGroup)
        session.commit()

        res.status = falcon.HTTP_200
        res.body = json.dumps(newGroup.toDict())
