from datetime import datetime
from enum import Enum as PyEnum

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, ForeignKey, Integer, String, DateTime, Text, Boolean, Enum
from sqlalchemy.orm import relationship

from .auid import genAuidFactory

ModelBase = declarative_base()

user_group_association = Table("user_group_associations", ModelBase.metadata,
    Column("userAuid", String(95), ForeignKey("users.userAuid"), nullable = False),
    Column("groupAuid", String(95), ForeignKey("groups.groupAuid"), nullable = False),
    Column("isGroupAdmin", Boolean, default = False, nullable = False)
)

class Group(ModelBase):
    __tablename__ = "groups"

    groupAuid      = Column(String(95), primary_key = True, nullable = False, default = genAuidFactory("GROUP"))
    name           = Column(String(31), nullable = False, unique = True)
    comment        = Column(Text, nullable = False)
    users          = relationship("UserAccount", secondary = user_group_association, back_populates = "groups")

    def toDict(self):
        keys = ("groupAuid", "name", "comment")
        return { k: getattr(self, k) for k in keys }

class UserAccount(ModelBase):
    __tablename__ = "users"

    userAuid       = Column(String(95), primary_key = True, nullable = False, default = genAuidFactory("USER"))
    email          = Column(String(95), unique = True, nullable = False)
    password       = Column(String(255), nullable = False)
    createdAt      = Column(DateTime, nullable = False, default = datetime.now)
    activatedAt    = Column(DateTime, nullable = True, default = None)
    lastModifiedAt = Column(DateTime, nullable = False, default = datetime.now)
    isDisabled     = Column(Boolean, nullable = False, default = False)
    realName       = Column(String(255), nullable = False)
    nickName       = Column(String(95), nullable = False, unique = True)
    groups         = relationship("Group", secondary = user_group_association, back_populates = "users")

    def toDict(self):
        keys = ("userAuid", "email", "password", "createdAt", "activatedAt", "lastModifiedAt",
                "isDisabled", "realName", "nickName")
        return { k: getattr(self, k) for k in keys }

#class AuditLog(ModelBase):
#    __tablename__ = "audit_log"
#
#    logAuid        = Column(String, primary_key = True, nullable = False, default = genAuidFactory("LOGENTRY"))
#    accountAuid    = ForeignKey()
#    eventAuid      = ()
#    eventTime      = Column(DateTime, nullable = False, default = datetime.now)
#    eventType      = ()
#    ipAddress      = Column(String, nullable = False)
#    userAgent      = Column(String, nullable = False)
#    infoString     = Column(String, nullable = False)

#class UserDetails(object):
#    pass

class DeveloperAccountStatus(PyEnum):
    InReview  = 1
    Approved  = 2
    Rejected  = 3
    Reapplied = 4

class DeveloperAccount(ModelBase):
    __tablename__ = "developer_metadata"

    devAccountAuid = Column(String(95), primary_key = True, nullable = False, default = genAuidFactory("DEVACCOUNT"))
    userAuid       = Column(String(95), ForeignKey("users.userAuid"), nullable = False)
    user           = relationship("UserAccount", foreign_keys = [ userAuid ], uselist = False, backref = "devMeta")
    supporterAuid  = Column(String(95), ForeignKey("users.userAuid"), nullable = True)
    supporter      = relationship("UserAccount", foreign_keys = [ supporterAuid ], backref = "supportedDevAccounts")
    specialReason  = Column(Boolean, nullable = False, default = False)
    justification  = Column(Text, nullable = False)
    evidenceLinks  = Column(Text, nullable = False)
    status         = Column(Enum(DeveloperAccountStatus), nullable = False, default = DeveloperAccountStatus.InReview)
    statusAt       = Column(DateTime, nullable = False, default = datetime.now)
    statusComment  = Column(String(255), nullable = False)

class EvMemberStatus(PyEnum):
    Active        = 1
    Extraordinary = 2

class EvMemberAccount(ModelBase):
    __tablename__ = "ev_metadata"

    userAuid       = Column(String(95), ForeignKey("users.userAuid"), nullable = False)
    evMemberAuid   = Column(String(95), primary_key = True, nullable = False, default = genAuidFactory("EVMEMBER"))
    evStatus       = Column(Enum(EvMemberStatus), nullable = False, default = EvMemberStatus.Extraordinary)

class SshKey(ModelBase):
    __tablename__ = "ssh_keys"

    userAuid       = Column(String(95), ForeignKey("users.userAuid"), nullable = False)
    user           = relationship("UserAccount", backref = "sshKeys")
    keyAuid        = Column(String(95), primary_key = True, nullable = False, default = genAuidFactory("SSHKEY"))
    keyData        = Column(Text, nullable = False)
    keyDescription = Column(String(255), nullable = False)
    keyFingerMD5   = Column(String(33), nullable = False, unique = True)
    keyFingerSHA2  = Column(String(44), nullable = False, unique = True)
