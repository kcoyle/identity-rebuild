import requests
import json

from sqlalchemy import create_engine

from .models import ModelBase

class AusweisAPI(object):
    def __init__(self, cfgFile):
        self._session = requests.Session()
        self._session.headers.update({"Content-Type": "application/json"})

        with open(cfgFile) as f:
            self._config = json.load(f)
        self._endpoint = self._config.get("apiRoot", "http://localhost:51145")

    def createDatabase(self):
        print("> Creating database tables...")

        dbUser = self._config.get("dbUser", "Ausweis")
        dbPassword = self._config.get("dbPassword", "Ausweis")
        dbHost = self._config.get("dbHost", "localhost")
        dbName = self._config.get("dbName", "ausweis")
        dbUri = f"mysql+pymysql://{dbUser}:{dbPassword}@{dbHost}/{dbName}"

        engine = create_engine(dbUri)
        ModelBase.metadata.create_all(engine)

        print("  Done")

    def createGroup(self, data):
        url = "".join((self._endpoint, "/groups"))
        r = self._session.post(url, data = json.dumps(data))
