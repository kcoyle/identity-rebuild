#!/usr/bin/env python3

import requests
import json

from ausweis.authapi import AusweisAPI
api = AusweisAPI("config.json")

# Step 1: Create the database
api.createDatabase()

# Step 2: Creata initial groups
print("> Creating initial groups")
with open("data/00-initial-groups.json") as f:
    r = json.loads(f.read())
groups = [ { "name": g, "comment": r[g] } for g in r.keys() ]
for g in groups:
    api.createGroup(g)
print("  Done")
