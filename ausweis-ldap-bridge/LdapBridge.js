/* Copyright 2018 Boudhayan Gupta <bgupta@kde.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const fs      = require("fs");
const ldap    = require("ldapjs");
const request = require("request");
const config  = require("./config.json")

// ------------------------------------------------------------------------------
// GROUPS
// ----- Schema: ----------------------------------------------------------------
//          dn: cn=<groupname>,ou=groups,dc=kde,dc=org
//          cn: Group name
//      member: Array of dn's of all member users
//   memberUid: Array of nicknames (uid's) of all member users
// ------------------------------------------------------------------------------

const loadGroupsList = (req, res, next) => {
    req.groups = {}
    request(config.apiRoot + config.apiEndpointGroups, (err, res, body) => {
        if (err) {
            return next(new ldap.OperationsError(err.message));
        }

        let resp = JSON.parse(body)
        resp.forEach((el) => {
            req.groups[el.name] = {
                dn: "cn=" + el.name + "," + config.basednGroups,
                attributes: {
                    cn: el.name,
                    member: [],
                    memberUid: [],
                    objectclass: "ausweisGeneric"
                }
            };
        });
        return next();
    });
};

// ------------------------------------------------------------------------------
// USERS
// ----- Schema: ----------------------------------------------------------------
//           dn: uid=<username>,ou=people,dc=kde,dc=org
//           cn: givenName space sn
//          uid: Textual username
//    givenName: First Name
//           sn: Surname
//        email: E-Mail address
//  groupMember: Array of group names
// ------------------------------------------------------------------------------

/*
const loadUsersList = (req, res, next) => {
    req.users = {}
    request(config.apiRoot + config.apiEndpointUsers, (err, res, body) => {
        if (err) {
            return next(new ldap.OperationsError(err.message));
        }

        let resp = JSON.parse(body)
        resp.forEach((el) => {
            req.users[el.name] = {
                dn: "uid=" + el.name + "," + config.basednGroups,
                attributes: {
                    cn: el.name,
                    member: [],
                    memberUid: [],
                    objectclass: "ausweisGeneric"
                }
            };
        });
        return next();
    });
};
*/

function loadUsersList(req, res, next) {
  fs.readFile("/etc/passwd", "utf8", function(err, data) {
    if (err)
      return next(new ldap.OperationsError(err.message));

    req.users = {};

    var lines = data.split("\n");
    for (var i = 0; i < lines.length; i++) {
      if (!lines[i] || /^#/.test(lines[i]))
        continue;

      var record = lines[i].split(":");
      if (!record || !record.length)
        continue;

      req.users[record[0]] = {
        dn: "uid=" + record[0] + ", ou=people, dc=kde, dc=org",
        attributes: {
          cn: record[0],
          uid: record[2],
          gid: record[3],
          description: record[4],
          homedirectory: record[5],
          shell: record[6] || "",
          objectclass: ["unixUser", "abracadabra"]
        }
      };
    }

    return next();
  });
}

// ------------------------------------------------------------------------------
// AUTHENTICATION
// ---- Allowed Users: ----------------------------------------------------------
// This is a read-only bridge, and therefore supports only one user in specified
// in the configuration file. This user is not available in the LDAP tree at all.
// ------------------------------------------------------------------------------

const ldapAuth = (req, res, next) => {
    if (!req.connection.ldap.bindDN.equals(config.ldapUser)) {
        return next(new ldap.InsufficientAccessRightsError());
    }
    return next();
}

// ------------------------------------------------------------------------------
// SERVER
// ------------------------------------------------------------------------------

var ldapServer = ldap.createServer();

ldapServer.bind(config.ldapUser, (req, res, next) => {
    if (req.dn.toString() !== config.ldapUser ||
        req.credentials !== config.ldapPassword) {
        return next(new ldap.InvalidCredentialsError());
    }

    res.end();
    return next();
});

ldapServer.search(config.basednGroups, [ ldapAuth, loadGroupsList ], (req, res, next) => {
    Object.keys(req.groups).forEach((el) => {
        if (req.filter.matches(req.groups[el].attributes)) {
            res.send(req.groups[el])
        }
    });

    res.end();
    return next();
});

ldapServer.search(config.basednUsers, [ ldapAuth, loadUsersList ], (req, res, next) => {
    Object.keys(req.users).forEach((el) => {
        if (req.filter.matches(req.users[el].attributes)) {
            res.send(req.users[el]);
        }
    });

    res.end();
    return next();
});

ldapServer.listen(config.listenPort, config.listenHost, () => {
    console.log("KDE Ausweis LDAP Bridge is running at: %s", ldapServer.url);
});
