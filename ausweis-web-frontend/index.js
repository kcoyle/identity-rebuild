/* Copyright 2018 Boudhayan Gupta <bgupta@kde.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const path     = require("path");
const express  = require("express");
const nunjucks = require("nunjucks");
const config   = require("./config.json");

// initialise our application
const app = express();

// Set up our static assets serving
const staticAssetsPath = path.join(__dirname, config.paths.staticAssets);
app.use("/static", express.static(staticAssetsPath));

// Set up templating
const templatesPath = path.join(__dirname, config.paths.templates)
nunjucks.configure(templatesPath, {
    autoescape: true,
    express: app,
    noCache: true
});

// views
app.get("/", (req, res) => {
    res.render("login.html.njk", {
        app: config.app
    });
});


// registration
app.get("/register/step01", (req, res) => {
    res.render("register_01_coc.html.njk", {
        app: config.app
    });
});

app.listen(3000, () => console.log("Listening..."));
